<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 30/06/16
 * Time: 9:42
 */
include 'latte.php';
include 'spainLatte.php';
include 'italianLatte.php';
$makeCoffee= new \Latte\MakeCoffee();
$coffee=$makeCoffee->getTypeofCoffee();
$addons=$makeCoffee->getAddon();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Your order</title>
</head>
<body>
<h1> Check your order</h1>
<table>
    <tr class="table-head">
        <th>Ingredient</th>
        <th>Price&nbsp;&euro;</th>
    </tr>
    <tr>
        <td>
            <p>Milk</p>
        </td>
        <td>
            <p><?php echo $coffee->getMilkPrice()?></p>
        </td>
    </tr>
    <?php
    if($addons && is_array($addons))
    {

        foreach ($addons as $addon)
        {
            echo('<tr><td>'.$addon->getType().':&nbsp;'.$addon->getName().'</td><td>'.$addon->getPrice().'</td></tr>');
        }
    }
    ?>
    <tr>
        <td>
            <p>Base Price</p>
        </td>
        <td>
            <p><?php echo $coffee->getBasePrice()?></p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Tax(<?php echo ($coffee->getTax()*100)?>%)</p>
        </td>
        <td>
            <p><?php echo ($coffee->taxValue($addons));?></p>
        </td>
    </tr>
    <tr class="total-price">
        <th>
            <p>Total</p>
        </th>
        <th>
            <p><?php echo ($coffee->getFullPrice($addons));?>&nbsp;&euro;</p>
        </th>
    </tr>

</table>
<a href="index.php">Home</a>
</body>
</html>
