<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/06/16
 * Time: 16:12
 */

namespace italianLatte;


use Latte\Ingredient;
use Latte\Latte;

class ItalianLatte extends Latte
{
    /**
     * @return mixed
     */
    private $milkPrice=0.4;
    private $portion=1;
    private $syrupPrice=0.5;
    private $basePrice=1.5;
    private $tax=0.04;
    
    public function __construct()
    {
        $complement= new Ingredient('Complement','Croissant',1);
        $syrup= new Ingredient('Syrup','Coconut',$this->syrupPrice);
        parent::__construct($complement,$syrup,$this->portion,$this->milkPrice,$this->basePrice,$this->tax);
        
    }


    
    

}