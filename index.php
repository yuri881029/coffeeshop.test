<?php
include "latte.php";
include "italianLatte.php";
include "spainLatte.php";
$spanishLatte= new \spainLatte\SpainLatte();
$italianLatte= new \italianLatte\ItalianLatte();

?>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <link rel="stylesheet" href="assets/style.css">
    <script src="assets/main.js"></script>
    <meta charset="UTF-8">
    <title>Coffee Shop.php</title>
</head>
<body>
<h1> Select a country shop</h1>
<form id="shopForm" action="makeCoffee.php" method="post">
    <label for="select-coffee">Select your coffee</label>
    <select id="select-coffee" class="select-coffee" name="selectlatte" onchange=selectCoffee(); required>
        <option value="">[Select Latte]</option>
        <option value="<?php echo(get_class($spanishLatte))?>">Spanish Latte</option>
        <option value="<?php echo(get_class($italianLatte))?>">Italian Latte</option>
    </select>
    <div class="make-coffee">
        <div class="coffee-menu spain-latte not-visited" data-selection=<?php echo(get_class($spanishLatte))?> >
            <ul>
                <li>
                    <p><?php echo($spanishLatte->numOfPortion())?>&nbsp;portion of coffee</p>
                </li>
                <li>
                    <p>Milk&nbsp;&nbsp;&nbsp;
                        <strong class="set-price" data-price="<?php echo $spanishLatte->getMilkPrice()?>">
                            <?php echo $spanishLatte->getMilkPrice()?>&euro;
                        </strong>
                    </p>
                </li>
                <li>
                    <label for="spanish-syrup"><?php echo($spanishLatte->getSyrup()->getName())?>&nbsp;Syrup</label>&nbsp;&nbsp;
                    <strong><?php echo($spanishLatte->getSyrup()->getPrice())?>&euro;</strong>
                    <input id="spanish-syrup" type="checkbox" name="addon['syrup']" class="select-addon select-syrup" value="syrup" data-price="<?php echo $spanishLatte->getSyrup()->getPrice()?>">
                    <br>
                </li>
                <li>
                    <label for="spanish-complement"><?php echo($spanishLatte->getComplement()->getName())?>&nbsp;as complement&nbsp;&nbsp;
                        <strong >
                            <?php echo($spanishLatte->getComplement()->getPrice())?>&euro;</strong>
                    </label>
                    <input id="spanish-complement" type="checkbox" name="addon['complement']" class="select-addon select-complement" data-price="<?php echo $spanishLatte->getComplement()->getPrice()?>" value="complement">
                </li>
                <li>
                    <p>Base Price&nbsp;&nbsp;&nbsp;<strong  class="base-price set-price" data-price="<?php echo $spanishLatte->getBasePrice()?>" data-portion="<?php echo($spanishLatte->numOfPortion())?>"><?php echo $spanishLatte->getBasePrice()?>&euro;(*<?php echo($spanishLatte->numOfPortion())?>)</strong></p>
                </li>
                <li>
                    <p>Country Tax&nbsp;&nbsp;&nbsp;<strong  class="set-tax" data-tax="<?php echo $spanishLatte->getTax()?>"><?php echo ($spanishLatte->getTax()*100)?>%</strong></p>
                </li>
            </ul>
        </div>
        <div class="coffee-menu italian-latte not-visited" data-selection=<?php echo(get_class($italianLatte))?> >
            <ul>
                <li>
                    <p><?php echo($italianLatte->numOfPortion())?>&nbsp;portion of coffee</p>
                </li>
                <li>
                    <p>Milk&nbsp;&nbsp;&nbsp;
                        <strong class="set-price" data-price="<?php echo $italianLatte->getMilkPrice()?>">
                            <?php echo $italianLatte->getMilkPrice()?>&euro;
                        </strong>
                    </p>
                </li>
                <li>
                    <label for="italian-syrup"><?php echo($italianLatte->getSyrup()->getName())?>&nbsp;Syrup</label>&nbsp;&nbsp;
                    <strong><?php echo($italianLatte->getSyrup()->getPrice())?>&euro;</strong>
                    <input id="italian-syrup" type="checkbox" name="addon['syrup']" class="select-addon select-syrup" value="syrup" data-price="<?php echo $italianLatte->getSyrup()->getPrice()?>">
                    <br>
                </li>
                <li>
                    <label for="select-complement"><?php echo($italianLatte->getComplement()->getName())?>&nbsp;as complement&nbsp;&nbsp;
                        <strong >
                            <?php echo($italianLatte->getComplement()->getPrice())?>&euro;</strong>
                    </label>
                    <input id="select-complement" type="checkbox" name="addon['complement']" class="select-addon select-complement" data-price="<?php echo $italianLatte->getComplement()->getPrice()?>" value="complement">
                </li>
                <li>
                    <p>Base Price&nbsp;&nbsp;&nbsp;<strong  class="base-price set-price" data-price="<?php echo $italianLatte->getBasePrice()?>" data-portion="<?php echo($italianLatte->numOfPortion())?>"><?php echo $italianLatte->getBasePrice()?>&euro;(*<?php echo($italianLatte->numOfPortion())?>)</strong></p>
                </li>
                <li>
                    <p>Country Tax&nbsp;&nbsp;&nbsp;<strong  class="set-tax" data-tax="<?php echo $italianLatte->getTax()?>"><?php echo ($italianLatte->getTax()*100)?>%</strong></p>
                </li>
            </ul>
        </div>
        <div class="show-price-div">
            <strong>Total Price&nbsp;</strong><span class="show-price"></span>&euro;
        </div>
        <div class="submit-btn">
            <input class="submit" type="submit" value="Make Coffee">
        </div>
    </div>
</form>
</body>

</html>