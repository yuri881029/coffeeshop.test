<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/06/16
 * Time: 16:12
 */

namespace spainLatte;


use Latte\Ingredient;
use Latte\Latte;

class SpainLatte extends Latte
{
    /**
     * @return mixed
     */
    private $milkPrice=0.3;
    private $portion=2;
    private $syrupPrice=0.5;
    private $basePrice=1;
    private $tax=0.03;

    public function __construct()
    {
        $complement= new Ingredient('Complement','Chocolate bar',1);
        $syrup= new Ingredient('Syrup','Melon',$this->syrupPrice);
        parent::__construct($complement,$syrup,$this->portion,$this->milkPrice,$this->basePrice,$this->tax);
        
    }
    
    

    
    

}