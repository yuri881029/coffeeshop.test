<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/06/16
 * Time: 15:09
 */

namespace Latte;
class Ingredient
{
    private $type;
    private $name;
    private $price;

    public function __construct($type,$name,$price)
    {
        $this->type=$type;
        $this->name=$name;
        $this->price=$price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

}

class Latte
{
    private $portion;
    private $milkPrice;
    private $basePrice;
    private $complement;
    private $syrup;
    private $tax;


    /**
     * Latte constructor.
     * @param Ingredient $complement
     * @param Ingredient $syrup
     * @param $portion
     * @param $milkPrice
     * @param $basePrice
     * @param $tax
     */
    public function __construct(Ingredient $complement, Ingredient $syrup, $portion, $milkPrice, $basePrice, $tax)
    {
        $this->portion=$portion;
        $this->milkPrice=$milkPrice;
        $this->basePrice=$basePrice;
        $this->complement=$complement;
        $this->syrup=$syrup;
        $this->tax=$tax;

    }

    /**
     * @return mixed
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    public function numOfPortion()
    {
        return $this->portion;
    }
    /**
     * @return Ingredient|null
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @return mixed
     */
    public function getMilkPrice()
    {
        return $this->milkPrice;
    }

    /**
     * @return Ingredient|null
     */
    public function getSyrup()
    {
        return $this->syrup;
    }


    /**
     *
     * @param int $addon
     * @return mixed
     */
    public function priceNotTax($addon=0)
    {
        $coffeePrice= $this->numOfPortion() * $this->basePrice;
        return $coffeePrice+$this->milkPrice+$addon;
    }
    
    public function taxValue($ingredients)
    {
        $addonPrice=$this->getAddonPrice($ingredients);
        $price=$this->priceNotTax($addonPrice);
        $result=$price*$this->tax;
        return $result;
    }

    /**
     * @param $ingredients
     * @return int|number
     */
    public function getAddonPrice($ingredients)
    {
        $addon=0;
        if(!empty($ingredients) && is_array($ingredients))
        {
            $sumPrice=[];
            foreach ($ingredients as $ingredient)
            {
                array_push($sumPrice, $ingredient->getPrice());
            }
            $addon=array_sum($sumPrice);
        }
        return $addon;
    }

    public function getFullPrice($ingredients)
     {
         $addonPrice=$this->getAddonPrice($ingredients);
         $priceNotTax=$this->priceNotTax($addonPrice);
         $result=$priceNotTax+($priceNotTax*$this->getTax());

       return number_format($result,2);
    }


    public function getIngredient($ingredient)
    {
        return $this->$ingredient;
    }
}

class MakeCoffee
{
    public function getRequest()
    {
        return $_REQUEST;
    }

    /**
     * @return void | mixed
     */
    public function getTypeofCoffee()
    {
        $result=null;
        $request=$this->getRequest();
        if(!empty($request) && !is_null($request))
        {
            $result=new $request['selectlatte'];

        }
        return $result;
    }

    /**
     * 
     * @return array
     */
    public function getAddon()
    {
        $request=$this->getRequest();
        $coffee=$this->getTypeofCoffee();
        $addons=[];
        if(isset($request['addon']))
        {
            foreach ($request['addon'] as $item)
            {
                array_push($addons, $coffee->getIngredient($item));
            }
        }



        return $addons;
    }

}