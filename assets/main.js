/**
 * Created by yuri on 4/07/16.
 */
function sum(a,b) {
    return parseFloat(a)+parseFloat(b);
}
function sumArray(array) {
    return array.reduce(function(result, a){return sum(result,a);});
}
function removeChecked(a)
{
    if(a && a.type ==='checkbox')
    a.checked= false;
}
function hideDivbyClass(c) {
    var a=document.getElementsByClassName(c);
    if(a)
    {
        for(var i=0;i < a.length;i++)
        {
            a[i].hidden=true;
        }
    }

}
function showDivbyClass(c) {
    var a=document.getElementsByClassName(c);
    if(a)
    {
        for(var i=0;i < a.length;i++)
        {
            a[i].hidden=false;
        }
    }

}

function calulatePrice() {
    var a=document.querySelector('.make-coffee');
    if(!a.hidden)
    {
        var menu=a.getElementsByClassName('coffee-menu');
        for(var i=0;i<menu.length;i++)
        {

            if(!menu[i].hidden)
            {
                var price=menu[i].getElementsByClassName('set-price');
                var tax=menu[i].querySelector('.set-tax').dataset.tax;
                var opt=menu[i].getElementsByClassName('select-addon');
                var showPrice=document.querySelector('.show-price');
                var taxValue=0;
                var t=[];
                if(opt)
                {
                    for(var x=0;x<opt.length;x++)
                    {
                        if(opt[x].checked)
                        {
                            var val=opt[x].dataset.price;
                            t.push(val);
                        }

                    }
                }
                for (var c=0;c<price.length;c++)
                {
                    var value=price[c].dataset.price;
                    if(price[c].dataset.portion)
                    {
                        value=value * price[c].dataset.portion;
                    }
                    t.push(value);

                }
                var n=sumArray(t);
                taxValue=parseFloat(tax)*n;
                var total=n+taxValue;
                showPrice.textContent=total.toFixed(2).toString();

            }
        }

    }

}
function selectCoffee()
{
    var value=document.getElementById('select-coffee').value;
    var a= document.getElementsByClassName('coffee-menu');
    if(value && a)
    {
        showDivbyClass('make-coffee');

        for(var i=0;i < a.length;i++)
        {
            if(a[i].dataset.selection===value)
            {
                if(a[i].classList.contains('not-visited')){
                    a[i].classList.remove('not-visited');
                }
                if(a[i].hidden)
                {
                    a[i].hidden=false;

                }
            }
            else{
                a[i].hidden=true;
                var input= a[i].getElementsByClassName('select-addon');
                for(var c=0;c < input.length;c++)
                {
                    removeChecked(input[c]);
                }
            }

        }
    }
    else
    {
        hideDivbyClass('make-coffee');

    }

}

window.addEventListener("load",selectCoffee,false);
window.addEventListener("change",calulatePrice,false);